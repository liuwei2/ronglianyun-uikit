package com.yuntongxun.kitsdk.listener;

import com.yuntongxun.ecsdk.ECMessage;

public interface IReceiveMessageListener {
    void onReceivedMessage(ECMessage message);
}
