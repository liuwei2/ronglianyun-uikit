package com.yuntongxun.kitsdk.listener;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.yuntongxun.ecsdk.ECMessage;
import com.yuntongxun.kitsdk.ui.chatting.holder.BaseHolder;

public interface ECUIkitListener {
    void queryUserInfo(String target, IUserInfoOk userInfoOk);

    void showNotification(ECMessage msg);

    void onTopHeadClicked(Activity activity, ImageView imageView, String target);

    void onClickOtherHead(final Context context,
                          final BaseHolder baseHolder, final ECMessage detail, final int position);

    void onClickMyHead(final Context context,
                       final BaseHolder baseHolder, final ECMessage detail, final int position);
}
