package com.yuntongxun.kitsdk.view;

public abstract class CustomerKit {
    private String targetId;

    public CustomerKit(String targetId) {
        this.targetId = targetId;
    }
    public abstract void getUserInfo();

    public abstract void getUserInfoOk(String nickname, String headUrl);
}
