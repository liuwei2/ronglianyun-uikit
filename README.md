#ronglianyun-uikit
我的荣联云uikit拓展,android studio arr库,可以自定义昵称和头像.
##使用方法
###版本2.x在程序的入口处调用
```java
ECDeviceKit.getInstance().setListener(new ECUIkitListener() {
            @Override
            public void queryUserInfo(String targetId, IUserInfoOk iUserInfoOk) {
                String nickname=//你根据容联云targetId查询到的昵称
                String headUrl=//你根据容联云targetId查询到的头像url
                iUserInfoOk.ok(nickname, headUrl);
            }

            @Override
            public void showNotification(ECMessage ecMessage) {

            }

            @Override
            public void onTopHeadClicked(Activity activity, ImageView imageView, String username) {

            }

            @Override
            public void onClickOtherHead(Context context, BaseHolder baseHolder, ECMessage ecMessage, int i) {
                String targetId=ecMessage.getForm();
            }

            @Override
            public void onClickMyHead(Context context, BaseHolder baseHolder, ECMessage ecMessage, int i) {
                String targetId=ecMessage.getForm();
            }
});
```
###版本1.x在程序的入口处调用
```java
ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
ECDeviceKit.getInstance().setGetUserInfo(new IGetUserInfo() {
	@Override
	public void query(String targetId, final IUserInfoOk iUserInfoOk) {
		String nickname=//你根据容联云targetId查询到的昵称
		String headUrl=//你根据容联云targetId查询到的头像url
		iUserInfoOk.ok(nickname, headUrl);
	}
});
```